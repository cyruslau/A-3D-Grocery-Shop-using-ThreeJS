# A-3D-Grocery-Shop-using-ThreeJS

[![Deploy static content to Pages](https://github.com/cyruslauwork/A-3D-Grocery-Shop-using-ThreeJS/actions/workflows/static.yml/badge.svg)](https://github.com/cyruslauwork/A-3D-Grocery-Shop-using-ThreeJS/actions/workflows/static.yml)

Demo Video: https://www.youtube.com/watch?v=mO_ScP_HDs8

Static Website (no backend connection): https://cyruslauwork.github.io/A-3D-Grocery-Shop-using-ThreeJS/

Credit List:

- Muhammed Erdem https://github.com/muhammed/vue-interactive-paycard

- Jamie Coulter https://codepen.io/jcoulterdesign/pen/ryzvqG
